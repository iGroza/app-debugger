import { AppDebugger, INetworkData } from 'mobile-app-debugger';
import React from 'react';
import { StyleSheet, Text, Button as RNButton, View, Alert } from 'react-native';

// configure debbuger
AppDebugger.configure({
  port: 9000,
  isOverwriteConsole: true
});

export default class App extends React.Component {

  state = {
    isRenderProblremComponent: false,
  }

  componentDidCatch(error: Error) {
    // send error to debbuger client when component did catch error
    AppDebugger.error({
      error: error,
      message: `Error from componentDidCatch ${error.message}`,
      meta: {
        userId: '00000001',
        userName: 'test user',
      }
    });

    this.setState({ isRenderProblremComponent: false });
  }

  private _testPostRequest() {
    ApiService.post('https://jsonplaceholder.typicode.com/posts', {
      title: 'foo',
      body: 'bar',
      userId: 1,
    });
  }

  private async _testGetRequest() {
    const post = await ApiService.get('https://jsonplaceholder.typicode.com/posts');
  }

  render() {
    const { isRenderProblremComponent } = this.state;
    return (
      <View style={styles.container}>
        {isRenderProblremComponent && <ProblemComponent />}
        <Text>This an example for 'mobile-app-debugger'!</Text>
        <Button onPress={() => this.setState({ isRenderProblremComponent: true })}>
          Test error
        </Button>
        <Button onPress={this._testPostRequest}>
          Test network POST request
        </Button>
        <Button onPress={this._testGetRequest}>
          Test network GET request
        </Button>
        <Button onPress={() => { AppDebugger.log('test log') }}>
          Test log
        </Button>
        <Button onPress={() => { AppDebugger.logWarn('test warn') }}>
          Test logWarn
        </Button>
        <Button onPress={() => { AppDebugger.logError('test error') }}>
          Test logError
        </Button>
        <Button onPress={() => { console.log('test console log', { magic: 42 }) }}>
          Test console.log
        </Button>
      </View>
    );
  }
}

function ProblemComponent() {
  return <Text>{noSuchObject.someVariable}</Text>
}

/* 
*  Best practies, create your api service, and add 
*/
class ApiService {
  static async post(url: string, data: any) {
    const headers = {
      'Content-type': 'application/json; charset=UTF-8',
    }

    const res = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headers
    })

    const resData = await res.json();

    AppDebugger.network({
      url: url,
      method: 'post',
      status: res.status,

      reqData: data,
      reqHeaders: headers,

      resData: resData,
      resHeaders: res.headers
    })

    return resData;
  }

  static async get(url: string) {
    const headers = {};

    const res = await fetch(url);

    const resData = await res.json();

    AppDebugger.network({
      url: url,
      method: 'get',
      status: res.status,

      reqHeaders: headers,

      resData: resData,
      resHeaders: res.headers
    })

    return resData;
  }
}

interface IButtonProps {
  children: string;
  onPress(): void;
}

function Button({ onPress, children }: IButtonProps) {
  return (
    <View style={{ marginTop: 10 }}>
      <RNButton title={children} onPress={onPress} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
